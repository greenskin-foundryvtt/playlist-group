## 1.0.0

### New Features

- Create Playlist Groups and link other playlists to them to have a single stream of tracks
