# Playlist Group

This module allows you to create playlist groups, i.e. playlists that group all the tracks
from other playlists in a single stream

# Authors

Skeroujvapluvit

## Supported language

- EN
- FR

## System and dependencies

None

## Usage

### Create a Playlists Group

A dedicated button for creating a playlist group is in the header of the Playlists pane

![Creer un groupe de playlists](media/CreatePlaylists.png)

When creating a playlist group you have to choose a name. It is also possible from the creation
to link playlists to this group

![Configurer un groupe de playlists](media/ConfigPlaylists.png)

After the creation, it is possible to configure the other playlist settings. A playlist group works exactly like a
playlist except that instead of adding audio tracks to it, it is associated with other playlists

Note that the playlist group is identified by a dedicated icon

![Configurer la playlists](media/ConfigPlaylists2.png)

A playlist group automatically contains all the tracks of the linked playlists and this list is automatically updated
when changes are made (modification of the linked lists, addition/removal of tracks in the linked playlists).

![Groupe de Playlists](media/PlaylistGroup.png)

From the context menu of a playlist group it is possible to edit its name and associated playlists

![Configurer la playlists](media/ConfigPlaylists3.png)

The following functions are not available on a playlist group:

- Duplicate
- Export data
- Import data

The following functions are not available on a track of a playlist group:

- Edit track
- Delete track
