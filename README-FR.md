# Playlist Group

Ce module permet de créer des groupes de playlists, c'est à dire des playlists qui regroupe toutes les pistes
d'autres playlists dans un flux unique

# Authors

Skeroujvapluvit

## Supported language

- EN
- FR

## System and dependencies

Aucune

## Usage

### Créer un groupe de Playlists

Un bouton dédié à la création d'un groupe de playlists est en en-tête du volet Playlists

![Creer un groupe de playlists](media/CreatePlaylists.png)

Lors de la création d'un groupe de playlists il faut à minima choisir un nom. Il est également possible dé la création
de lier des playlists à ce groupe

![Configurer un groupe de playlists](media/ConfigPlaylists.png)

Après la création, il est possible de configurer les autres paramètre de playlists. Un groupe de playlists fonctionne
exactement comme une playlist à l'exeption qu'au lieu de lui ajouter des pistes audio, on lui associe d'autre playlists

On remarque que le groupe de playlists est identifié par une icone dédié

![Configurer la playlists](media/ConfigPlaylists2.png)

Un groupe de playlists contient automatiquement toutes les pistes des playlists liés et cette listes est automatiquement
mise à jour en cas de changement (modification des listes lié, ajout/suppression de pistes dans les playlists liés)

![Groupe de Playlists](media/PlaylistGroup.png)

Depuis le menu contextuel d'un groupe de playlists il est possible d'éditer son nom et les playlists associés

![Configurer la playlists](media/ConfigPlaylists3.png)

Les fonctions suivantes ne sont pas disponibles sur un groupe de playlists :

- Dupliquer
- Exporter les donnée
- Importer les donnée

Les fonctions suivantes ne sont pas disponible sur une piste d'un groupe de playlists :

- Éditer la piste
- Supprimer la piste
