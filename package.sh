#!/bin/bash
set -e

rm -fr dist
npm run build
cp -r $MODULE_DIRS dist/
export MODULE_VERSION=$(jq ".version" package.json | sed -r 's/["]+//g')
envsubst '${MODULE_VERSION}' < ./module.json > ./dist/module.json
