import { i18nLocalize, initTemplates, isGM, playlists, user } from './constant';
import { PlaylistGroup } from './playlist-group';
import { PlaylistGroupChooser } from './playlist-group-chooser';

Hooks.once('init', () => {
  initTemplates([
    'modules/playlist-group/templates/playlist-group-chooser.html',
    'modules/playlist-group/templates/chooser-action.html',
  ]);
});

Hooks.once('ready', async () => {
  for (const playlist of playlists().contents) {
    if (playlist?.flags?.playlistGroup) {
      await PlaylistGroup.updatePlaylistGroupSounds(playlist);
    }
  }
});

Hooks.on('getPlaylistDirectoryEntryContext', (_, options) => {
  options.push({
    name: i18nLocalize('PLAYLISTGROUP.playlists.group.edit'),
    condition: isGM(),
    icon: '<i class="fas fa-edit fa-fw"></i>',
    callback: async (target) => {
      const id: string = target.attr('data-document-id') ?? target.parent().attr('data-document-id');
      if (id != null) {
        const playlist: Playlist = playlists().contents.find(
          (pl) => pl._id === id
        );
        if ((<any>playlist)?.flags?.playlistGroup) {
          await PlaylistGroupChooser.selectPlaylists(
            playlist.name ?? '',
            (<any>playlist)?.flags?.playlists ?? [],
            async (name, ids) => {
              await playlist.update({
                name: name,
                flags: {
                  playlists: ids,
                },
              });
              await PlaylistGroup.updatePlaylistGroupSounds(playlist);
            }
          );
        }
      }
    },
  });
});

Hooks.on('renderPlaylistConfig', (_, html: JQuery) => {
  const id = html[0].id.replace('playlist-config-', '');
  if (id != null) {
    const playlist = playlists().contents.find((pl) => pl._id === id);
    if (playlist?.flags?.playlistGroup) {
      html[0].classList.add('playlist-group-config');
      html.find('.form-group').last().addClass('playlist-group-hide');
    }
  }
});

Hooks.on('createPlaylistSound', async (sound: PlaylistSound) => {
  await PlaylistGroup.updatePlaylistGroupFromSound(sound);
});

Hooks.on('deletePlaylistSound', async (sound: PlaylistSound) => {
  await PlaylistGroup.updatePlaylistGroupFromSound(sound);
});

Hooks.on(
  'renderPlaylistDirectory',
  async (_: PlaylistDirectory, html: JQuery) => {
    if (user().can('ACTOR_CREATE')) {
      addPlaylistGroupActionButton(
        html,
        'PLAYLISTGROUP.common.add.button',
        async (_) => {
          await PlaylistGroup.createPlaylistGroup();
        }
      );
    }

    html.find('.playlist-header').each((_, elm) => {
      const id = elm.dataset.documentId;
      if (id != null) {
        const playlist = playlists().contents.find((pl) => pl._id === id);
        if (playlist?.flags?.playlistGroup) {
          html
            .find(elm)
            .children()
            .first()
            .before('<i class="fas fa-file-audio playlist-group-icon"></i>');
          elm?.parentElement?.classList?.add('playlist-group');
          html.find(elm).find('a[data-action="sound-create"]').remove();
        } else {
          elm.classList.add('playlist-legacy');
        }
      }
    });
  }
);

// const nativeCollapse = PlaylistDirectory['prototype']['_collapse'];
//
// PlaylistDirectory['prototype']['_collapse'] = (el, collapse, speed = 250) => {
//   console.log(`collapse : ${collapse}`)
//   nativeCollapse(el, collapse, speed)
// }

function addPlaylistGroupActionButton(
  html: JQuery,
  label: string,
  onClick: (event: MouseEvent) => void
) {
  const button = document.createElement('button');
  button.style.width = '95%';
  button.innerHTML = i18nLocalize(label);
  button.addEventListener('click', (event) => {
    onClick(event);
  });
  html.find('.header-actions').after(button);
}
