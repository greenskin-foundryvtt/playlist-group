export const user = () => (<any>game).user;
export const i18n = () => (<any>game).i18n;
export const i18nLocalize = (id: string) => i18n().localize(id);
export const playlists = () => (<any>game).playlists;
export const initTemplates = (paths: string[]) => loadTemplates(paths);
export const isGM = () => user()?.isGM ?? false;
