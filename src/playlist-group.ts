import { PlaylistGroupChooser } from './playlist-group-chooser';
import { playlists } from './constant';
import { PlaylistSoundData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs';
import { RandomUtil } from './random-util';
import { DocumentData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/module.mjs';

export class PlaylistGroup {
  public static async createPlaylistGroup() {
    await PlaylistGroupChooser.selectPlaylists('', [], async (name, ids) => {
      const playlistGroup:
        | StoredDocument<Playlist>
        | undefined = await Playlist.create({
        name: name,
        flags: {
          playlistGroup: true,
          playlists: ids,
        },
      });
      await this.updatePlaylistGroupSounds(playlistGroup ?? <Playlist>{});

      playlistGroup?.sheet?.render(true);
    });
  }

  public static async updatePlaylistGroupFromSound(sound: PlaylistSound) {
    const playlist: Playlist | null = sound.parent;
    if (playlist != null) {
      const playlistGroups = playlists().contents.filter(
        (pl) =>
          pl?.flags?.playlistGroup &&
          pl?.flags?.playlists?.includes((<any>playlist)._id)
      );
      for (const plg of playlistGroups) {
        await this.updatePlaylistGroupSounds(plg);
      }
    }
  }

  public static async updatePlaylistGroupSounds(
    playlistGroup: Playlist | StoredDocument<Playlist>
  ) {
    const playListIds: string[] = (<any>playlistGroup)?.flags?.playlists ?? [];
    await playlistGroup.deleteEmbeddedDocuments(
      PlaylistSound.documentName,
      playlistGroup?.sounds?.contents?.map((s) => (<any>s)?._id ?? '')
    );
    const sounds: PlaylistSound[] = [];
    for (const pli of playListIds) {
      const playlist: Playlist = playlists().contents.find(
        (pl) => pl._id === pli
      );
      if (playlist != null) {
        for (const sound of playlist.sounds.contents) {
          const clonedSoundData: PlaylistSoundData & any = duplicate(sound);
          clonedSoundData._id = RandomUtil.getRandomId();
          sounds.push(clonedSoundData);
        }
      }
    }
    if (sounds.length > 0) {
      await playlistGroup.createEmbeddedDocuments(
        PlaylistSound.metadata.name,
        this.toRecords(sounds)
      );
    }
  }

  public static toRecords<E, F extends object>(
    arrays: (DocumentData<E, F> & any)[]
  ): Record<string, unknown>[] {
    return arrays.map((d) => (d.toObject != null ? d.toObject() : d));
  }
}
