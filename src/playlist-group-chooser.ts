import { AbstractChooser } from './abstract-chooser';
import { i18n, playlists } from './constant';
import { SelectModel } from './select-model';

class Model {
  public name: string;
  public playlistIds: string[];

  constructor(name: string, playlistIds: string[]) {
    this.name = name;
    this.playlistIds = playlistIds;
  }
}

export class PlaylistGroupChooser extends AbstractChooser<
  Model,
  {
    playlists: SelectModel[];
    selectedPlaylists: SelectModel[];
  }
> {
  private callback: (name: string, initPlaylistIds: string[]) => void;
  private selectedPlaylist: string;

  constructor(
    object: Model,
    callback: (name: string, initPlaylistIds: string[]) => void,
    options?: Partial<FormApplicationOptions>
  ) {
    super(object, options);
    const pls = playlists().contents.filter((pl) => !pl.flags?.playlistGroup);
    this.selectedPlaylist = pls.length > 0 ? pls[0]._id : '';
    this.model.playlists = pls.map(
      (pl) => new SelectModel(pl._id, pl.name, this.selectedPlaylist === pl._id)
    );
    this.updateSelectedPlaylists();
    this.callback = callback;
  }

  public static get defaultOptions(): FormApplicationOptions {
    return mergeObject(super.defaultOptions, {
      id: 'playlist-group-chooser',
      title: i18n().localize('PLAYLISTGROUP.playlists.group.edit'),
      template: `modules/playlist-group/templates/playlist-group-chooser.html`,
      width: 400,
    });
  }

  public static async selectPlaylists(
    initName: string,
    initPlaylistIds: string[],
    callback: (name: string, initPlaylistIds: string[]) => void
  ) {
    new PlaylistGroupChooser(
      new Model(initName, initPlaylistIds),
      callback
    ).render(true);
  }

  public activateListeners(html: JQuery) {
    super.activateListeners(html);
    this.handleChange(html, '#nameInput', (event) => {
      this.updateName(event.currentTarget.value);
    });

    this.handleChange(html, '#selectPlaylists', (event) => {
      this.selectedPlaylist = event.currentTarget.value;
      this.model.playlists.forEach((pl) => {
        pl.selected = pl.key === this.selectedPlaylist;
      });
    });

    this.handleClick(html, '#addPlaylistButton', () => {
      if (!this.model.data.playlistIds.includes(this.selectedPlaylist)) {
        this.model.data.playlistIds.push(this.selectedPlaylist);
        this.updateSelectedPlaylists();
        this.render();
      }
    });

    this.handleClick(html, '.remove-playlist-button', (event) => {
      const key = event.currentTarget.value;
      if (this.model.data.playlistIds.includes(key)) {
        this.model.data.playlistIds = this.model.data.playlistIds.filter(
          (pli) => pli !== key
        );
        this.updateSelectedPlaylists();
        this.render();
      }
    });
  }

  protected isValid(data: Model): boolean {
    return data?.name?.length > 0;
  }

  protected yes(data: Model) {
    this.callback(data?.name, data?.playlistIds);
  }

  private updateName(name: string) {
    this.model.data.name = name;
  }

  private updateSelectedPlaylists() {
    this.model.selectedPlaylists = this.model.playlists.filter((pl) =>
      this.model.data.playlistIds.includes(pl.key)
    );
  }
}
